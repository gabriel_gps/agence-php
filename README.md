# Performance comercial

Single Page Application com Laravel no back-end e Vue.js no front-end.

## Tecnologias

- PHP >= 7.1.3
- Laravel
- JavasSript
- Vue.js
- Vue Material
- CSS / Sass
- Chartist.js
- Composer
- Artisan

## Instalação

``` sh
git clone https://bitbucket.org/gabriel_gps/agence-php.git
cd agence-php
composer install
```
É necessário criar o arquivo .env com os dados do banco de dados e a APP_KEY.
Uma vez criado o arquivo .env, a APP_KEY pode ser criada automaticamente com o comando:
``` sh
php artisan key:generate
```

## Instalação Front-end
``` sh
cd frontend
npm install
```

## Lançar aplicação
``` sh
# Lançar backend
php artisan serve

# Em outro terminal
cd frontend
npm run dev
```

## Rodar testes com PHPUnit
``` sh
# Instalação global
phpunit

# Instalação local - Linux/Mac
./vendor/bin/phpunit

# Instalação local - Windows
.\vendor\bin\phpunit
```
