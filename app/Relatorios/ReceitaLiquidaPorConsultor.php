<?php

namespace App\Relatorios;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class ReceitaLiquidaPorConsultor
{
    public function get(array $consultoresIds = [], Carbon $dt_inicio = null, Carbon $dt_fim = null)
    {
        $query = DB::table('cao_fatura');

        $query->select([
            'cao_os.co_usuario',
            DB::raw('DATE_FORMAT(cao_fatura.data_emissao, "%m/%Y") AS mes_referencia'),
            DB::raw('ROUND(SUM(cao_fatura.valor * (1 - cao_fatura.total_imp_inc/100)), 2) AS receita_liquida'),
            DB::raw('ROUND(SUM(cao_fatura.valor * (1 - cao_fatura.total_imp_inc/100)) * cao_fatura.comissao_cn/100, 2) AS comissao'),
            DB::raw('cao_salario.brut_salario AS custo_fixo'),
        ]);

        $query->join('cao_os', 'cao_fatura.co_os', '=', 'cao_os.co_os');
        $query->join('cao_salario', 'cao_salario.co_usuario', '=', 'cao_os.co_usuario');

        if (!empty($consultoresIds)) {
            $query->whereIn('cao_os.co_usuario', $consultoresIds);
        }

        if ($dt_inicio) {
            $query->where('cao_fatura.data_emissao', '>=', $dt_inicio->format('Y-m-01'));
        }

        if ($dt_fim) {
            $query->where('cao_fatura.data_emissao', '<=', $dt_fim->format('Y-m-t'));
        }

        $query->groupBy([
            'cao_os.co_usuario',
            DB::raw('DATE_FORMAT(cao_fatura.data_emissao, "%m/%Y")')
        ]);

        $result = $query->get()->groupBy('co_usuario');


        return $this->formatResult($result);
    }

    private function formatResult($result)
    {
        $formated_data = collect();

        foreach ($result as $key => $value) {
            $formated_data[] = [
                'consultor_id' => $key,
                'data' => $value,
            ];
        }

        return $formated_data;
    }
}
