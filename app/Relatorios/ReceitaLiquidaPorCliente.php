<?php

namespace App\Relatorios;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class ReceitaLiquidaPorCliente
{
    public function get(array $clientesIds = [], Carbon $dt_inicio = null, Carbon $dt_fim = null)
    {
        $query = DB::table('cao_fatura');

        $query->select([
            'cao_fatura.co_cliente',
            DB::raw('DATE_FORMAT(cao_fatura.data_emissao, "%m/%Y") AS mes_referencia'),
            DB::raw('ROUND(SUM(cao_fatura.valor * (1 - cao_fatura.total_imp_inc/100)), 2) AS receita_liquida'),
        ]);

        if (!empty($clientesIds)) {
            $query->whereIn('cao_fatura.co_cliente', $clientesIds);
        }

        if ($dt_inicio) {
            $query->where('cao_fatura.data_emissao', '>=', $dt_inicio->format('Y-m-01'));
        }

        if ($dt_fim) {
            $query->where('cao_fatura.data_emissao', '<=', $dt_fim->format('Y-m-t'));
        }

        $query->groupBy([
            'cao_fatura.co_cliente',
            DB::raw('DATE_FORMAT(cao_fatura.data_emissao, "%m/%Y")')
        ]);

        return $query->get();
    }
}
