<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    const ADMIN = 0;
    const CONSULTA = 1;
    const APOIO = 2;

    /**
     * Scope para incluir apenas consultores.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    /*public function scopeConsultor($query)
    {
        $query->join('permissao_sistema', 'cao_usuario.co_usuario', '=', 'permissao_sistema.co_usuario');

        $query->where('permissao_sistema.co_sistema', 1);
        $query->where('permissao_sistema.in_ativo', 'S');

        $query->whereIn('permissao_sistema.co_tipo_usuario', [
            self::ADMIN,
            self::CONSULTA,
            self::APOIO
        ]);

        return $query;
    }*/
}
