<?php

namespace App\Models\Cliente;

use Illuminate\Support\Facades\DB;

/**
 *
 */
class ClientesRepository
{

    public function get()
    {
        $query = DB::table('cao_cliente');

        $query->select([
            'cao_cliente.co_cliente',
            'cao_cliente.no_fantasia',
        ]);

        $query->where('cao_cliente.tp_cliente', 'A');

        $query->orderBy('cao_cliente.no_fantasia');

        return $query->get();
    }
}
