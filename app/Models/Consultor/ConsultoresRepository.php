<?php

namespace App\Models\Consultor;

use App\Usuario;
use Illuminate\Support\Facades\DB;

/**
 *
 */
class ConsultoresRepository
{

    public function get()
    {
        $query = DB::table('cao_usuario');

        $query->select([
            'cao_usuario.co_usuario',
            'cao_usuario.no_usuario',
        ]);

        $query->join('permissao_sistema', 'cao_usuario.co_usuario', '=', 'permissao_sistema.co_usuario');

        $query->where('permissao_sistema.co_sistema', 1);
        $query->where('permissao_sistema.in_ativo', 'S');

        $query->whereIn('permissao_sistema.co_tipo_usuario', [
            Usuario::ADMIN,
            Usuario::CONSULTA,
            Usuario::APOIO
        ]);

        $query->orderBy('cao_usuario.no_usuario');

        return $query->get();
    }
}
