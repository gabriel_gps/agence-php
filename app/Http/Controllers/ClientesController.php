<?php

namespace App\Http\Controllers;

use App\Models\Cliente\ClientesRepository;

class ClientesController extends Controller
{
    public function index(ClientesRepository $clientesRepo)
    {
        return $clientesRepo->get();
    }
}
