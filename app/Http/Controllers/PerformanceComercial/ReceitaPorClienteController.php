<?php

namespace App\Http\Controllers\PerformanceComercial;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Relatorios\ReceitaLiquidaPorCliente;
use App\Http\Requests\ReceitaPorClienteRequest;

class ReceitaPorClienteController extends Controller
{
    protected $relatorio;

    public function __construct(ReceitaLiquidaPorCliente $relatorio)
    {
        $this->relatorio = $relatorio;
    }

    public function getReceitaLiquida(ReceitaPorClienteRequest $request)
    {
        $clientes = [];
        $dt_inicio = $dt_fim = null;

        if ($request->filled('clientes')) {
            $clientes = $request->input('clientes');
        }

        if ($request->filled('dt_inicio')) {
            $dt_inicio = Carbon::createFromFormat('m/Y', $request->input('dt_inicio'));
        }
        if ($request->filled('dt_fim')) {
            $dt_fim = Carbon::createFromFormat('m/Y', $request->input('dt_fim'));
        }

        return $this->relatorio->get($clientes, $dt_inicio, $dt_fim);
    }
}
