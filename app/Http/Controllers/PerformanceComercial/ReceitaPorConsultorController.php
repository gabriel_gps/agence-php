<?php

namespace App\Http\Controllers\PerformanceComercial;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Relatorios\ReceitaLiquidaPorConsultor;
use App\Http\Requests\ReceitaPorConsultorRequest;

class ReceitaPorConsultorController extends Controller
{
    protected $relatorio;

    public function __construct(ReceitaLiquidaPorConsultor $relatorio)
    {
        $this->relatorio = $relatorio;
    }

    public function getReceitaLiquida(ReceitaPorConsultorRequest $request)
    {
        $consultores = [];
        $dt_inicio = $dt_fim = null;

        if ($request->filled('consultores')) {
            $consultores = $request->input('consultores');
        }

        if ($request->filled('dt_inicio')) {
            $dt_inicio = Carbon::createFromFormat('m/Y', $request->input('dt_inicio'));
        }
        if ($request->filled('dt_fim')) {
            $dt_fim = Carbon::createFromFormat('m/Y', $request->input('dt_fim'));
        }

        return $this->relatorio->get($consultores, $dt_inicio, $dt_fim);
    }
}
