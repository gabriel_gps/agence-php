<?php

namespace App\Http\Controllers;

use App\Models\Consultor\ConsultoresRepository;

class ConsultoresController extends Controller
{
    public function index(ConsultoresRepository $consultoresRepo)
    {
        return $consultoresRepo->get();
    }
}
