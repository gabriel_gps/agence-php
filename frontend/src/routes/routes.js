import DashboardLayout from "@/pages/Layout/DashboardLayout.vue";

import Login from "../pages/Login.vue";
import Dashboard from "@/pages/Dashboard.vue";
import EmConstrucao from "@/pages/EmConstrucao.vue";
import PerformanceComercial from "@/pages/PerformanceComercial.vue";
import ConsultoresTab from '../components/ConsultoresTab.vue';
import ClientesTab from '../components/ClientesTab.vue';

import RelatorioConsultor from '../components/Consultor/Relatorio.vue';
import GraficoConsultor from '../components/Consultor/Grafico.vue';
import PizzaConsultor from '../components/Consultor/Pizza.vue';

import RelatorioCliente from '../components/Cliente/Relatorio.vue';
import GraficoCliente from '../components/Cliente/Grafico.vue';
import PizzaCliente from '../components/Cliente/Pizza.vue';

/*
import UserProfile from "@/pages/UserProfile.vue";
import TableList from "@/pages/TableList.vue";
import Typography from "@/pages/Typography.vue";
import Icons from "@/pages/Icons.vue";
import Maps from "@/pages/Maps.vue";
import Notifications from "@/pages/Notifications.vue";
import UpgradeToPRO from "@/pages/UpgradeToPRO.vue";
*/

const routes = [
  {
    component: Login,
    path: "/login",
  },
  {
    path: "/",
    component: DashboardLayout,
    redirect: "/performance-comercial",
    children: [
      {
        path: "em-construcao1",
        component: EmConstrucao
      },
      {
        path: "em-construcao2",
        component: EmConstrucao
      },
      {
        path: "em-construcao3",
        component: EmConstrucao
      },
      {
        path: "em-construcao4",
        component: EmConstrucao
      },
      {
        path: "em-construcao5",
        component: EmConstrucao
      },
      {
        path: "performance-comercial",
        component: PerformanceComercial,
        children: [
          {
            path: "/",
            redirect: "por-consultor",
          },
          {
            path: "por-consultor",
            name: "Performance comercial > Por Consultor",
            component: ConsultoresTab,
            children: [
              /*{
                path: "/",
                redirect: "relatorio",
              },*/
              {
                path: "relatorio",
                name: "Performance comercial > Por Consultor > Relatório",
                component: RelatorioConsultor
              },
              {
                path: "grafico",
                name: "Performance comercial > Por Consultor > Gráfico",
                component: GraficoConsultor
              },
              {
                path: "pizza",
                name: "Performance comercial > Por Consultor > Pizza",
                component: PizzaConsultor
              }
            ]
          },
          {
            path: "por-cliente",
            name: "Performance comercial > Por Cliente",
            component: ClientesTab,
            children: [
              /*{
                path: "/",
                redirect: "relatorio",
              },*/
              {
                path: "relatorio",
                name: "Performance comercial > Por Cliente > Relatório",
                component: RelatorioCliente
              },
              {
                path: "grafico",
                name: "Performance comercial > Por Cliente > Gráfico",
                component: GraficoCliente
              },
              {
                path: "pizza",
                name: "Performance comercial > Por Cliente > Pizza",
                component: PizzaCliente
              }
            ]
          },
        ]
      },
      /*
      {
        path: "dashboard",
        name: "Dashboard",
        component: Dashboard
      },
      {
        path: "user",
        name: "User Profile",
        component: UserProfile
      },
      {
        path: "table",
        name: "Table List",
        component: TableList
      },
      {
        path: "typography",
        name: "Typography",
        component: Typography
      },
      {
        path: "icons",
        name: "Icons",
        component: Icons
      },
      {
        path: "maps",
        name: "Maps",
        meta: {
          hideFooter: true
        },
        component: Maps
      },
      {
        path: "notifications",
        name: "Notifications",
        component: Notifications
      },
      {
        path: "upgrade",
        name: "Upgrade to PRO",
        component: UpgradeToPRO
      }
      */
    ]
  }
];

export default routes;
