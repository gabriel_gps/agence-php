export default class ClienteService {

  constructor(resource) {

    const customActions = {};

    this._resource = resource('clientes{/id}', {}, customActions);
  }

  list(params) {
    return this._resource
      .get(params)
      .then(res => res.json());
  }
}
