export default class RelatorioReceitaLiquidaService {

  constructor(resource) {

    const customActions = {};

    this._resource = resource('performance-comercial/por-consultor/receita-liquida', {}, customActions);
  }

  list(params) {
    return this._resource
      .get(params)
      .then(res => res.json());
  }
}
