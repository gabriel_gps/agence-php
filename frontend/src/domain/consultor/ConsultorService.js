export default class ConsultorService {

  constructor(resource) {

    const customActions = {};

    this._resource = resource('consultores{/id}', {}, customActions);
  }

  list(params) {
    return this._resource
      .get(params)
      .then(res => res.json());
  }
}
