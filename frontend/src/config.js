import Vue from 'vue';

Vue.mixin({
  filters: {
    floatToMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
      try {
        decimalCount = Math.abs(decimalCount);
        decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

        const negativeSign = amount < 0 ? "-" : "";

        let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
        let j = (i.length > 3) ? i.length % 3 : 0;

        return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
      } catch (e) {
        console.log(e)
      }
    },
    floatToMoney_old(value) {
      if ( !value ) {
          return value;
      }

      value = parseFloat(value).toFixed(2);

      value = value.toString();

      var parts = value.split('.');

      if ( 1 === parts.length ) {
          value += '.00';
      }
      else {
          if ( 1 === parts[1].length ) {
              value += '0';
          }
          else if ( 0 === parts[1].length ) {
              value += '0';
          }
      }

      return value.replace('.', ',');
    },
  }
});
