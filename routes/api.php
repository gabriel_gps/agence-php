<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/consultores', 'ConsultoresController@index');
Route::get('/clientes', 'ClientesController@index');

Route::namespace('PerformanceComercial')->prefix('performance-comercial')->group(function () {
    Route::get('/por-consultor/receita-liquida', 'ReceitaPorConsultorController@getReceitaLiquida');
    Route::get('/por-cliente/receita-liquida', 'ReceitaPorClienteController@getReceitaLiquida');
});
