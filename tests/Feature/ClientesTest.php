<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientesTest extends TestCase
{
    /**
     * @test
     */
    public function listagem_retorna_a_estrutura_correta()
    {
        $response = $this->get('/api/clientes');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'co_cliente',
                    'no_fantasia',
                ]
            ]);;
    }

    /**
     * @test
     */
    public function receita_liquida_por_cliente_retorna_a_estrutura_correta()
    {
        $response = $this->get('/api/performance-comercial/por-cliente/receita-liquida');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'co_cliente',
                    'mes_referencia',
                    'receita_liquida',
                ]
            ]);;
    }
}
