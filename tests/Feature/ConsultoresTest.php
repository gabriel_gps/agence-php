<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConsultoresTest extends TestCase
{
    /**
     * @test
     */
    public function listagem_retorna_a_estrutura_correta()
    {
        $response = $this->get('/api/consultores');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'co_usuario',
                    'no_usuario',
                ]
            ]);;
    }

    /**
     * @test
     */
    public function receita_liquida_por_consultor_retorna_a_estrutura_correta()
    {
        $response = $this->get('/api/performance-comercial/por-consultor/receita-liquida');

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'consultor_id',
                    'data' => [
                        [
                            'co_usuario',
                            'mes_referencia',
                            'receita_liquida',
                            'comissao',
                            'custo_fixo',
                        ]
                    ],
                ]
            ]);;
    }
}
